from django.shortcuts import render, get_object_or_404
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers
from .forms import NoteForm
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.http import response

# Create your views here.
def index(request) : 
    notes =  Note.objects.all()
    response = {'notes':notes}
    form = NoteForm(request.POST or None)
      
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return response.HttpResponseRedirect('/lab-5')
  
    response['form']= form

    return render(request, 'lab5_index.html', response)

def update_note(request) : 
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None)
      
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return response.HttpResponseRedirect('/lab-5')
  
    context['form']= form
    return render(request, 'lab5_form.html', context)

# def note_update(request, pk) : 
#     note = get_object_or_404(Note, pk=pk)
#     if request.method == 'POST':
#         form = NoteForm(request.POST, instance=note)
#     else:
#         form = NoteForm(instance=note)
#     return save_note_form(request, form, 'notes/includes/partial_note_update.html')

# def save_note_form(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             data['form_is_valid'] = True
#             notes = Note.objects.all()
#             data['html_note_list'] = render_to_string('notes/includes/partial_note_list.html', {
#                 'notes': notes
#             })
#         else:
#             data['form_is_valid'] = False
#     context = {'form': form}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)


def copas(request) : 
    notes =  Note.objects.all()
    response = {'notes':notes}
    return render(request, 'copas.html', response)
# def get_note(request, id):
#     # dictionary for initial data with
#     # field names as keys
#     context ={}
 
#     # add the dictionary during initialization
#     context["data"] = Note.objects.get(id = id)
#     render(request, 'lab5.json', context)

#     data = serializers.serialize('json', context["data"])
    
#     return HttpResponse(data, content_type="application/json")

def get_note(request, id):
    context = {}
    notes = Note.objects.all()
    if request.method == 'POST':
    #  and request.is_ajax():
        ID = request.POST.get('id')
        note = notes.get(id=ID) # So we send the company instance
        context['note'] = note
    context['notes'] = notes
    context["note"] = Note.objects.get(id = id)
    render(request,'lab5.json',context)

    # data = serializers.serialize('json', context["notes"])
    data = serializers.serialize("json", Note.objects.filter(pk=id))
    
    return HttpResponse(data, content_type="application/json")
    # context = {}
    # notes = Note.objects.all()
    # # add the dictionary during initialization
    # context["note"] = Note.objects.get(id = id)
    # ID = request.POST.get('id')
    # note = notes.get(id=ID)
    # context['note'] = note
    # context['notes'] = notes
    # render(request,'lab5.json',context)

    # data = serializers.serialize('json', context["note"])
    
    # return HttpResponse(data, content_type="application/json")
         
    # return render(request, "lab5_index.html", context)
