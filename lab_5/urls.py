from django.urls import path
from .views import index, copas, get_note

app_name = "lab-5"

urlpatterns = [
    path('', index, name='index'),
    path('copas', copas, name='copas'),
    path('view/<int:id>', get_note, name='edit'),
    

]
