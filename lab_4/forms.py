from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm) : 
    

    class Meta:
        model = Note
        fields = ['to', 'var_from', 'title', 'message']

    error_messages = {
        'required' : 'Please Type'
    }
    
    to_attrs = {
        'type' : 'text',
        'placeholder' : 'Ketikkan nama penerima',
        'size' : 30
    }
    to = forms.CharField(label='To ', required=True, max_length=50, widget=forms.TextInput(attrs=to_attrs))

    from_attrs = {
        'type' : 'text',
        'placeholder' : 'Ketikkan nama pengirim',
        'size' : 30
    }
    var_from = forms.CharField(label='From ', required=True, max_length=50, widget=forms.TextInput(attrs=from_attrs))

    title_attrs = {
        'type' : 'text',
        'placeholder' : 'Ketikkan judul pesan',
        'size' : 30
    }
    title = forms.CharField(label='Title ', required=True, max_length=50, widget=forms.TextInput(attrs=title_attrs))