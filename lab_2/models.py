from django.db import models

# Create your models here.
class Note(models.Model) : 
    to = models.CharField(max_length=10)
    var_from = models.CharField(max_length=10)
    title = models.CharField(max_length=30)
    message = models.TextField(max_length=200)
