Apakah perbedaan antara JSON dan XML?
- JSON merupakan format data, sedangkan XML adalah bahasa markup.
- JSON merupakan format pertukaran data yang lebih ringan dan mudah bagi
  komputer untuk mengurai data yang sedang dikirim
- XML digunakan untuk menyimpan dan mengirimkan data dari satu aplikasi
  ke aplikasi lain melalui internet
- JSON tidak menggunakan tag, sedangkan XML menggunakan tag yang kita buat 
  sendiri
- Data dalam JSON disimpan dengan key dan value, sedangkan data dalam XML
  disimpan dengan tree stucture

Apakah perbedaan antara HTML dan XML?
- XML berfokus pada transfer data, sedangkan HTML untuk penyajian data
- XML case sensitive, sedangkan HTML tidak
- XML memiliki tag yang tidak ditentukan sebelumnya, sedangkan HTML sudah mempunyai
  tag yang ditentukan