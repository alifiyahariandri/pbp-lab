from lab_1.models import Friend
from django import forms

class FriendFrom(forms.ModelForm) : 
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'dob']

    error_messages = {
        'required' : 'Please Type'
    }
    
    name_attrs = {
        'type' : 'text',
        'placeholder' : 'Ketikkan nama teman'
    }
    name = forms.CharField(label='Nama ', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))

    npm_attrs = {
        'type' : 'text',
        'placeholder' : 'Ketikkan 10 digit NPM'
    }
    npm = forms.CharField(label='NPM ', required=True, max_length=10, widget=forms.TextInput(attrs=npm_attrs))

    date_attrs = {
        'type' : 'date'
    }
    dob = forms.DateField(label='DOB ', required=True,  widget=forms.TextInput(attrs=date_attrs))