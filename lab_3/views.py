from django.http import response
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendFrom
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request) : 
    friends =  Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


def add_friend(request) : 
    context ={}
  
    # create object of form
    form = FriendFrom(request.POST or None)
      
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return response.HttpResponseRedirect('/lab-3')
  
    context['form']= form
    return render(request, 'lab3_form.html', context)