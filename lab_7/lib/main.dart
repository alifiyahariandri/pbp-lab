import 'package:flutter/material.dart';

const Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget { // stateless widget punya method yg namanya build yg harus dioverride di class turunannya
  @override
  Widget build(BuildContext context) { // ini yg bakal ditampilkan di layar
    return MaterialApp( // sebuah widget yg berisi data2 yg diperlukan oleh aplikasi yg menggunakan material design
      home: MyHomePage(),
      
      debugShowCheckedModeBanner: false,
      
      title: 'PeduliLindungi2.0',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
        primaryColor: Colors.white,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),


    );
  }
}

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.lightBlue,
            child: Text(
              'Profil',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Daftar Vaksin', Icons.coronavirus, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Log Out', Icons.logout, () {
//             Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
        ],
      ),
    );
  }
}


class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  var pertanyaan = "";
  var judul = "";
  
  bool validate() {
    bool status = false;
    final form = _formKey.currentState;
    form?.save();
    if (form!.validate()) {
      form.save();
      status = true;
    } else {
      status = false;
    }
    return status;
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      appBar: AppBar(),
      drawer: MainDrawer(),
      backgroundColor: Colors.lightBlue,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 40.0),
          Padding (
            padding: EdgeInsets.only(left: 30.0),
            child: Column (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget> [
                Text("Selamat datang di",
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.normal,
                    )
                ),
                Text("PeduliLindungi2.0",
                      style: TextStyle(
                      fontFamily: "Montserrat",
                      color: Colors.white,
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ]
            )
          ),
          SizedBox(height:40.0),
          Container(
            height: MediaQuery.of(context).size.height - 100,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
            ),
            child: ListView(
              primary: false,
              padding: EdgeInsets.only(left:40.0, right:25.0),
              children: <Widget>[
                SizedBox(height:50.0),
                Row (
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget> [
     
                    Text("Forum",
                     style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      )
                    ),
                    IconButton(
                      icon: Icon(Icons.add_circle),
                      onPressed:() {},
                      color: Colors.lightBlue,
                    ),
                    
                  ]
                ),

                Padding (
                  padding: EdgeInsets.only(top: .0),
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250.0,
                    child: Container(
                      child: ListView(
                        children: [

                          Form(
                            
                            key: _formKey,
                            child: Container(
                              padding: EdgeInsets.all(20.0),
                              child: Column(
                              children: [
              
                                TextFormField(
                                  decoration: new InputDecoration(
                                  hintText: "Masukkan judul forum",
                                  labelText: "Judul Forum",
                                  icon: Icon(Icons.question_answer_outlined),
                                  border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(5.0)),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                     return 'Judul tidak boleh kosong';
                                  }
                                  judul = "$value";
                                  return null;
                                },
                              ),
                                TextFormField(
                                  decoration: new InputDecoration(
                                  hintText: "Masukkan pertanyaan Anda",
                                  labelText: "Pertanyaan",
                                ),
                              
                                validator: (value) {
                                  if (value!.isEmpty) {
                                     return 'Pertanyaan tidak boleh kosong';
                                  }
                                  pertanyaan = "$value";
                                  return null;
                                },
                              ),
                              SizedBox(height:25.0),
                              RaisedButton(
                                child: Text(
                                  "Kirim",
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: Colors.blue,
                                onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      print("Pertanyaan telah terkirim");
                                      print("Berikut merupakan forum baru yang Anda buat: ");
                                      print("Judul Forum : $judul");
                                      print("Pertanyaan  : $pertanyaan");
                                    }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                          
                          SizedBox(height:30.0),
                          
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget> [
                              Text("Tentang Vaksinasi",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                   )
                              ),
                              Text("Dibuat oleh alifiyah-ariandri pada 18 November 2021",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 11.0,
                                   )
                              ),
                              SizedBox(height:5.0),
                              Text("Tolong bantu saya memilih tempat vaksinasi terbaik di Jakarta")
                            ]
                          ),
                          SizedBox(height:30.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget> [
                              Text("Tentang Sertifikat",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                   )
                              ),
                              Text("Dibuat oleh alifiyah-nur pada 18 November 2021",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 11.0,
                                   )
                              ),
                              SizedBox(height:5.0),
                              Text("Saya bingung harus melihat sertifikat vaksin saya di mana")
                            ]
                          ),
                          SizedBox(height:30.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget> [
                              Text("Saya Bingung",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                   )
                              ),
                              Text("Dibuat oleh ali-ifiyah pada 18 November 2021",
                                   style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 11.0,
                                   )
                              ),
                              SizedBox(height:5.0),
                              Text("Saya bingung harus menanyakan apa, saya menulis ini biar bisa discroll aja")
                            ]
                          ),
                        ]
                      )  
                    )
                  )
                )
              ]
            )
          )
        ]
      )
    );
  }
}



